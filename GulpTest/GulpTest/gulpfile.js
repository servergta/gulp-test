﻿/// <binding AfterBuild='scripts' ProjectOpened='watch' />
// include plug-ins
var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var del = require('del');
var bs = require('browser-sync');

var config = {
    scriptsrc: ['app/**/*.js', '!app/**/*.min.js']
}

//delete the output file(s)
gulp.task('clean-scripts', function () {
    return del(['app/all.min.js']);
});

gulp.task('scripts', ['clean-scripts'], function () {

    return gulp.src(config.scriptsrc)
      .pipe(uglify())
      .pipe(concat('all.min.js'))
      .pipe(gulp.dest('app/'));
});

gulp.task('styles', ['clean-scripts'], function () {

    return gulp.src(config.scriptsrc)
      .pipe(uglify())
      .pipe(concat('all.min.js'))
      .pipe(gulp.dest('app/'));
});

gulp.task('default', ['scripts'], function () { });

gulp.task('watch', ['scripts', 'styles'], function () {
    gulp.watch(config.scriptsrc, ['scripts']);
    gulp.watch(config.scriptsrc, ['scripts']);
});