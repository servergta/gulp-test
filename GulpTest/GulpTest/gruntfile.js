﻿module.exports = function (grunt) {
    grunt.initConfig({
        uglify: {
            development: {
                options: {
                    sourceMap: true
                },
                files: [{
                    expand: true,
                    cwd: 'app/',
                    src: ['**/*.js', '!**/*.min.js', '!all-grunt.js'],
                    dest: 'app/',
                    ext: '.min.js'   
                }]
            },
            production: {
                options: {
                    compress: true
                },
                files: [{
                    expand: true,
                    cwd: 'app/webroot/js',
                    src: ['**/*.js', '!**/*.min.js', '!all-grunt.js'],
                    dest: 'app/',
                    ext: '.min.js'
                }]
            }
        },
        concat: {
            dist: {
                src: ['app/**/*.min.js'],
                dest: 'app/all-grunt.min.js',
            },
        },
        cssmin: {
            development: {
                target: {
                    files: [{
                        expand: true,
                        cwd: 'release/css',
                        src: ['*.css', '!*.min.css'],
                        dest: 'release/css',
                        ext: '.min.css'
                    }]
                }
            },
            production: {
                target: {
                    files: [{
                        expand: true,
                        cwd: 'release/css',
                        src: ['*.css', '!*.min.css'],
                        dest: 'release/css',
                        ext: '.min.css'
                    }]
                }
            }
        },
        watch: {
            scripts: {
                files: ['**/*.js', '!**/*.min.js', '!dependencies/**'],
                tasks: ['uglify:development']
            },
        }
    });
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default', ['uglify:development', 'watch']);
};